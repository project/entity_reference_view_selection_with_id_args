<?php

declare(strict_types=1);

namespace Drupal\entity_reference_view_selection_with_id_args\Plugin\EntityReferenceSelection;

use Drupal\Core\Form\FormStateInterface;
use Drupal\views\Plugin\EntityReferenceSelection\ViewsSelection;

/**
 * Plugin implementation of the 'selection' entity_reference.
 *
 * @EntityReferenceSelection(
 *   id = "view_selection_with_id_args",
 *   label = @Translation("Views: Filter by an entity reference view (with current entity ID as argument)"),
 *   group = "view_selection_with_id_args",
 *   weight = 0
 * )
 */
class ViewsSelectionWithIdArgs extends ViewsSelection {

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    if (isset($form['view']['arguments'])) {
      $form['view']['arguments']['#description'] = $this->t('Provide a comma separated list of additional arguments to pass to the view.<br />The current entity ID will always be the first argument passed to the view.');
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  protected function getDisplayExecutionResults(string $match = NULL, string $match_operator = 'CONTAINS', int $limit = 0, array $ids = NULL) {
    $display_name = $this->getConfiguration()['view']['display_name'];
    $arguments = $this->getViewArguments();
    $results = [];
    if ($this->initializeView($match, $match_operator, $limit, $ids)) {
      $results = $this->view->executeDisplay($display_name, $arguments);
    }
    return $results;
  }

  /**
   * Gets the arguments to pass to the view with the entity id as the first arg.
   *
   * @return array
   *   Array of arguments.
   */
  protected function getViewArguments(): array {
    $arguments = $this->getConfiguration()['view']['arguments'];
    /** @var \Drupal\Core\Entity\EntityInterface $entity */
    $entity = $this->getConfiguration()['entity'];
    array_unshift($arguments, $entity->id());
    return $arguments;
  }

}
